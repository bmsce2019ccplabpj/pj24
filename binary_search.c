#include<stdio.h>
int main()
{
int n,ele,pos=-1;
printf("Enter the number of elements in the array \n");
scanf("%d",&n);
int a[n],beg=0,mid,end=n;
printf("Enter the elements of the array \n");
for(int i=0;i<n;i++)
{
scanf("%d",&a[i]);
}
printf("Enter the search element \n");
scanf("%d",&ele);
while(beg<=end)
{
mid=(beg+end)/2;
if(a[mid]==ele)
{
pos=mid+1;
break;
}
else if(ele<a[mid])
{
end=mid-1;
}
else if(ele>a[mid])
{
beg=mid+1;
}

}
if (pos > -1)
{
printf("The position of the element %d in the array is %d \n",ele,pos);
}
else
{
printf("Element %d does not exist in the array \n",ele);
}
return 0;
}
