#include <stdio.h>
int main()
{
int n,pos,ele;
printf("Enter the number of elements in the array \n");
scanf("%d",&n);
int a[n];
 pos=-1;
printf("Enter the elements of the array \n");
for(int i=0;i<n;i++)
{
scanf("%d",&a[i]);
}
printf("Enter the element you want to search in the array \n");
scanf("%d",&ele);
for(int i=0;i<n;i++)
{
if(a[i]==ele)
{
pos=i;
break;
}
}
if(pos > -1)
{
printf("The element %d exists in the %d position \n",ele,pos);
}
else
{
printf("The element %d does not exist in the array \n",ele);
}
return 0;
}
