include <stdio.h>
int main()
{
    char ch;
    int numsum=0,uppersum=0,lowersum=0;
    do{
        printf("Enter a character enter * to stop counting \n ");
        scanf(" %c",&ch);
        if(ch>=48&&ch<=57)
            numsum+=1;
        else if(ch>=65&&ch<=90)
            uppersum+=1;
        else if(ch>=97&&ch<=122)
            lowersum+=1;
    }while(ch!='*');
    printf("The number of uppercase characters entered are %d \n",uppersum);
    printf("The number of lowercase characters entered are %d \n",lowersum);
    printf( "The number of numerical characters entered are %d \n",numsum);
    return 0;
}
